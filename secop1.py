
import csv
import json
from urllib.request import urlopen
from bs4 import BeautifulSoup
import time
import mysql.connector
from mysql.connector import errorcode
import mysql
from datetime import datetime
from datetime import timedelta
import logging





def funcionImpresionLogs(mensaje):
    logger1 =logging.getLogger()
    logger1.warning(mensaje)


def cargar_datos_mysql(array,cnx1):
  navegar = cnx1.cursor()
  #navegar.execute("""INSERT INTO wp-secop (id,link,sector,identificador,modalidad,estado,entidad,descripcion,region,monto,tipofecha,fecha,fechareplicas,fechaingreso) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""" ,(array[0], array[1], array[2],array[4], array[5], array[6], array[7],array[8], array[9], array[10], array[11], array[12]," ",array[3]))
  try:
      navegar.execute("""INSERT INTO secop2 (id,link,sector,secop,identificador,modalidad,estado,entidad,descripcion,region,monto,tipofecha,fecha,fechaingreso) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""" ,(array[0], array[1], array[2],array[4], array[5], array[6], array[7],array[8], array[9], array[10], array[11], array[12],array[13],array[3]))

  except:
      funcionImpresionLogs("Error al insertar un registro por duplicamiento");
  else:
      cnx1.commit()


def ajustarfechaformatomysql(fecha):
    nuevafecha = fecha[6:10]+"-"+fecha[3:5]+"-"+fecha[0:2]
    return nuevafecha

def darformatodenumeroMySqlalMonto(monto):
    largo= len(monto)
    montoMod=monto[1:largo]
    montoMod2 =montoMod.split(".")
    MontoMod3 =''.join(montoMod2)
    return MontoMod3

def arreglarID(monto):
    montoMod2 =monto.split("-")
    MontoMod3 = montoMod2[2]
    return MontoMod3

#configuracion de logs
logging.basicConfig(filename ='logs.log',level= logging.WARNING, format ='%(asctime)s:%(levelname)s:%(message)s')
logger =logging.getLogger()

#conexion a base de datos

try:
    cnx = mysql.connector.connect(user='root', password='',database='bitnami_wordpress')

except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        funcionImpresionLogs("Hay un problema con el usuario o password de la base de datos")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        funcionImpresionLogs("La base de datos no existe")
    else:
        funcionImpresionLogs(err)

else:
#ejecución del programa




    with open ('valores2.csv','r') as valores:
        lector = csv.DictReader(valores)


        for line in lector:
            try:
                html = urlopen(line['links'])
            except:
                logger.warning("No se pudo conectar con SECOP1")
            else:
                bs = BeautifulSoup(html, 'html.parser')
                table = bs.findAll('table') [0]
                rows = table.findAll('tr')
                csvFile = open('editors.csv', 'wt+')
                writer = csv.writer(csvFile)
                Bloque = []

                for row in rows:
                    csvRow = []

                    for cell in row.findAll(['a']):                          #Vamos a introducir todos los hipervinvulos de cada una de las procesos de contratacion
                        LargodeCadena = len(cell['href'])-2                  #Este largo de la cadena se le restan 2 para dejar lo que nos interesa del hipervinvulo
                        csvRow.append(arreglarID(cell['href'][LargodeCadena-15:LargodeCadena]))
                        csvRow.append("https://www.contratos.gov.co"+ cell['href'][24:LargodeCadena])
                        csvRow.append(line['codigo'] )
                        csvRow.append(time.strftime("%Y-%m-%d") )
                        csvRow.append("SECOP-UNO" )                                                                                    #se empieza desde 24 que es donde comienza la parte del hipervinculo que nos interesa
                        for cell in row.findAll(['td'])[1:7]:
                            csvRow.append(cell.get_text())
                        for cell in row.findAll(['td'])[7:8]:
                            csvRow.append(darformatodenumeroMySqlalMonto(cell.get_text()))
                        for cell in row.findAll(['td'])[8:9]:
                                                                   #en este espacio se separan el descriptivo de la fecha de la fecha
                            largo = len(cell.get_text())
                            largosinfecha = largo-10
                            limitesuperior =largosinfecha+10
                            csvRow.append(cell.get_text()[0:largosinfecha])
                            fechamod = cell.get_text()[largosinfecha:limitesuperior]
                            csvRow.append(ajustarfechaformatomysql(fechamod))


                    Bloque.append(csvRow)
                Bloquenuevo  = []   #crearunbloquenuevopara quitar las lineas en blanco
                for item in Bloque:
                    if item!=[]:
                        try:
                            TresMeses = datetime.now()-timedelta(days=90)   #TresMeses = datetime.date.today()-timedelta(days=90)
                            variableFecha = datetime.strptime(item[13],'%Y-%m-%d')
                            if variableFecha > TresMeses:
                                Bloquenuevo.append(item)
                                cargar_datos_mysql(item,cnx)
                            else:
                                pass
                        except:
                            Bloquenuevo.append(item)
                            cargar_datos_mysql(item,cnx)






            time.sleep(4)
        csvFile.close()
        cnx.close()
